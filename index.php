<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>
<body>
    <div class="form-container">

        <?php if(!isset($_SESSION['user'])): ?>
            <form action="./server.php" method="post">
                
                <label for="username">Username: </label>
                <input type="text" name="username" id="username" placeholder="example@email.com" required>

                <label for="password">Password: </label>
                <input type="password" name="password" id="password" required>

                <input type="submit" value="login" name='action'>
            </form>
        <? elseif(isset($_SESSION['user'])): ?>
            <?php foreach($_SESSION['user'] as $userdata): ?>
                <form action="./server.php" method="POST">
                    <p>Hello, <?=$userdata->$username?></p>
                    <input type="submit" name='action' value='logout'>
                </form>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

</body>
</html>