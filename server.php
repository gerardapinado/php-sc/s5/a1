<?php
session_start();

class User {
    private $username;
    private $password;

    function __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
    }


    function login($username, $password){
        
            $userData = (object)[
                'username' => $username,
                'password' => $password
            ];
        

        if($_SESSION['user'] === null){
            $_SESSION = array();
        }

        array_push($_SESSION['user'], $userData);
    }

    function logout(){
        session_destroy();
    }

}

$user1 = new User("johnsmith@gmail.com", "1234");

switch($_POST['action']){
    case 'login':
        if($_POST['username'] === $user1->$username && $_POST['password'] === $user1->password )
        $user1->login($_POST['username'], $_POST['password']);
        break;
    case 'logout':
        $user1->logout();
        break;
}

header('Location: ./index.php');